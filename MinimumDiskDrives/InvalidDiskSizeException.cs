﻿// Author: Ioannis Christodoulou
using System;

namespace MinimumDiskDrives
{
    /// <summary>
    /// Thrown if disk size gien is invalid.
    /// </summary>
    internal class InvalidDiskSizeException : Exception
    {
        internal InvalidDiskSizeException() : base() { }
        internal InvalidDiskSizeException(String message) : base(message) { }
        internal InvalidDiskSizeException(String message, Exception inner) : base(message, inner) { }
    }
}
