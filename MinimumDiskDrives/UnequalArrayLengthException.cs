﻿using System;

// Author: Ioannis Christodoulou
namespace MinimumDiskDrives
{
    /// <summary>
    /// Thrown when used and total element arrays length are not equal.
    /// </summary>
    internal class UnequalArrayLengthException : Exception
    {
        internal UnequalArrayLengthException() : base() { }
        internal UnequalArrayLengthException(String message) : base(message) { }
        internal UnequalArrayLengthException(String message, Exception inner) : base(message, inner) { }
    }
}
